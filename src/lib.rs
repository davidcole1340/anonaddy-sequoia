mod sequoia;
#[cfg(test)]
mod tests;

use ext_php_rs::{
    info_table_end, info_table_row, info_table_start,
    php::{module::ModuleEntry, types::binary::Binary},
    prelude::*,
};

use std::ops::Deref;

use openpgp::policy::{NullPolicy, Policy, StandardPolicy};
use sequoia_openpgp as openpgp;

use sequoia::*;

#[php_const]
const ANONADDY_SEQUOIA_ALLOW_WEAK_CRYPTO: i64 = 1;

#[php_function(optional = "options", defaults(options = 0))]
pub fn anonaddy_sequoia_encrypt(
    signing_cert: String,
    recipient_certs: Binary<u8>,
    content: String,
    options: i64,
) -> Result<String, String> {
    let policy: Box<dyn Policy> = if (options & ANONADDY_SEQUOIA_ALLOW_WEAK_CRYPTO) != 0 {
        Box::new(NullPolicy::new())
    } else {
        Box::new(StandardPolicy::new())
    };

    encrypt_for(signing_cert, recipient_certs.deref(), content, policy).map_err(|e| e.to_string())
}

#[php_function]
pub fn anonaddy_sequoia_merge(
    existing_cert: String,
    new_cert: String,
) -> Result<Binary<u8>, String> {
    merge_certs(existing_cert, new_cert)
        .map(Binary::new)
        .map_err(|e| e.to_string())
}

#[php_function(optional = "options", defaults(options = 0))]
pub fn anonaddy_sequoia_minimize(cert: String, options: i64) -> Result<Binary<u8>, String> {
    let policy: Box<dyn Policy> = if (options & ANONADDY_SEQUOIA_ALLOW_WEAK_CRYPTO) != 0 {
        Box::new(NullPolicy::new())
    } else {
        Box::new(StandardPolicy::new())
    };

    minimize_cert(cert, policy)
        .map(Binary::new)
        .map_err(|e| e.to_string())
}

#[no_mangle]
pub extern "C" fn php_module_info(_module: *mut ModuleEntry) {
    info_table_start!();
    info_table_row!("anonaddy sequoia extension", "enabled");
    info_table_end!();
}

#[php_module]
pub fn get_module(module: ModuleBuilder) -> ModuleBuilder {
    module.info_function(php_module_info)
}
