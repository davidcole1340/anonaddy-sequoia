use crate::sequoia::*;
use openpgp::{
    parse::Parse,
    policy::{NullPolicy, StandardPolicy},
    Cert,
};
use sequoia_openpgp as openpgp;

#[test]
fn end_to_end_test() -> openpgp::Result<()> {
    let existing_cert: String =
        String::from_utf8_lossy(include_bytes!("../wiktor-expired.asc")).to_string();
    let new_cert: String =
        String::from_utf8_lossy(include_bytes!("../wiktor-fresh.asc")).to_string();

    // merging certs should work
    let cert = merge_certs(existing_cert, new_cert)?;
    assert!(Cert::from_bytes(&cert).is_ok());

    // minimizing certs should provide a valid cert that is smaller than the input
    let minimized_cert = minimize_cert(&cert, Box::new(StandardPolicy::new()))?;
    assert!(Cert::from_bytes(&minimized_cert).is_ok());
    assert!(minimized_cert.len() < cert.len());

    // encrypting should still work
    let signing_cert = String::from_utf8_lossy(include_bytes!("../signing-key.asc")).to_string();
    let message = encrypt_for(
        &signing_cert,
        &minimized_cert,
        "just a random string",
        Box::new(StandardPolicy::new()),
    )?;
    assert!(openpgp::Message::from_bytes(message.as_bytes()).is_ok());

    Ok(())
}

#[test]
fn minimize() -> openpgp::Result<()> {
    let cert: String = String::from_utf8_lossy(include_bytes!("../wiktor-fresh.asc")).to_string();

    let minimized_cert = minimize_cert(&cert, Box::new(StandardPolicy::new()))?;
    assert!(Cert::from_bytes(&minimized_cert).is_ok());
    assert_eq!(2277, minimized_cert.len());

    Ok(())
}

#[test]
fn minimize_expired() -> openpgp::Result<()> {
    let cert: String = String::from_utf8_lossy(include_bytes!("../wiktor-expired.asc")).to_string();

    let minimized_cert = minimize_cert(&cert, Box::new(StandardPolicy::new()))?;
    let signing_cert = String::from_utf8_lossy(include_bytes!("../signing-key.asc")).to_string();
    let message = encrypt_for(
        &signing_cert,
        &minimized_cert,
        "random string",
        Box::new(StandardPolicy::new()),
    )?;
    assert!(openpgp::Message::from_bytes(message.as_bytes()).is_ok());
    Ok(())
}

#[test]
fn null_policy_test() -> openpgp::Result<()> {
    let cert: String = String::from_utf8_lossy(include_bytes!("../weak-key.asc")).to_string();
    assert!(minimize_cert(&cert, Box::new(StandardPolicy::new())).is_err());
    let minimized_cert = minimize_cert(&cert, Box::new(NullPolicy::new()))?;
    assert!(Cert::from_bytes(&minimized_cert).is_ok());

    let signing_cert = String::from_utf8_lossy(include_bytes!("../signing-key.asc")).to_string();
    assert!(encrypt_for(
        &signing_cert,
        &minimized_cert,
        "just a random string",
        Box::new(StandardPolicy::new())
    )
    .is_err());
    let message = encrypt_for(
        &signing_cert,
        &minimized_cert,
        "just a random string",
        Box::new(NullPolicy::new()),
    )?;
    assert!(openpgp::Message::from_bytes(message.as_bytes()).is_ok());

    Ok(())
}
